#ifndef LINKEDLIST_H
#define LINKEDLIST_H

typedef struct list
{
	int value;
	list * next;
} list;

list * newNode(int value);
void pushNewNode(list ** head, int value);
int popNode(list**head);
void printList(list * head);
bool isEmpty(list* head);
void deleteList(list** head);


#endif
