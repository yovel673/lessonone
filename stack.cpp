#include "stack.h"
#include <iostream>
#include "LinkedList.h"

void push(stack* s, unsigned int element)
{
	
	pushNewNode(&(s->head), element);
}

int pop(stack* s)
{
	return popNode(&(s->head));
}

void initStack(stack* s)
{
	s->head = NULL;

	
}
void cleanStack(stack* s)
{
	deleteList(&(s->head));
}

void printStack(stack *s)
{
	printList(s->head);
}

