#include "LinkedList.h"
#include <iostream>

list * newNode(int value)
{
	list * theNewNode = new list;
	theNewNode->value = value;
	theNewNode->next = NULL;
	return theNewNode;
}

void pushNewNode(list ** head, int value)
{
	list* newNode = new list;
	newNode->value = value;
	newNode->next = *(head);
	*head = newNode;

	std::cout << value << " pushed to stack\n";
}

int popNode(list** head)
{
	list *temp = *head;
	int popped = 0;
	if (isEmpty(*head))
	{
		std::cout << "the list is empty cannot pop\n";
		return -1;
	}
	*head = (*head)->next;
	popped = temp->value;
	delete(temp);
	std::cout << popped << " has been popped\n";
	return popped;

}

bool isEmpty(list* head)
{
	return !head;
}

void printList(list * head)
{
	list *current = head;
	if (current != NULL)
	{
		std::cout << "Stack: ";
		do
		{
			std::cout << current->value << " ->";
			current = current->next;
		} while (current != NULL);
		std::cout << "\n";
	}
	else
	{
		std::cout<<"The Stack is empty\n";
	}
}

void deleteList(list** head)
{
	
	list* current = *head;
	list* next;

	while (current != NULL)
	{
		next = current->next;
		delete(current);
		current = next;
	}

	
	*head = NULL;
}
