#include "queue.h"

#include <iostream>

void initQueue(queue* q, unsigned int size)
{
	q->arr = new int[size];
	q->first = 0;
	q->last = -1;
	q->size = 0;
	q->arrSize = size;
}

void cleanQueue(queue* q)
{
	delete[] q->arr;
	delete(q);
}

bool isEmpty(queue* q)
{
	if (q->size == 0)
	{
		return true;
	}
	else
	{
		return false;
	}
}

bool isFull(queue* q)
{
	if (q->size == q->arrSize)
	{
		return true;
	}
	else
	{
		return false;
	}
}

void enqueue(queue* q, unsigned int newValue)
{
	if (isFull(q))
	{
		std::cout << "Queue is Full!! Cannot insert value!\n";
	}
	else
	{
		std::cout << "Inserting " << newValue << "\n";
		q->last = (q->last + 1) % q->arrSize;
		q->arr[q->last] = newValue;
		(q->size)++;

	}
}

int dequeue(queue* q)
{
	int num = 0;
	if (isEmpty(q))
	{
		std::cout << "Queue is empty!! Cannot pop items!\n";
		return -1;
	}
	else
	{
		num = q->arr[q->first];
		q->first = (q->first + 1) % q->arrSize;
		(q->size)--;
		std::cout << num << " removed from queue! \n";
		return num;
	}
}




