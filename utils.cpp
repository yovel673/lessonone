#include "utils.h"
#include "stack.h"
#include <iostream>

void reverse(int* nums, unsigned int size)
{
	stack * arr = new stack;
	int i = 0;
	initStack(arr);
	for (i = 0; i < size; i++)
	{
		push(arr, nums[i]);
	}
	printStack(arr);
	for (i = 0; i < size; i++)
	{
		nums[i] = pop(arr);
	}
	cleanStack(arr);
}

int* reverse10()
{
	int * nums = new int[10];
	int i = 0;
	int input = 0;
	for (i = 0; i < 10; i++)
	{
		std::cout << "\nenter a number please: ";
		std::cin >> input;
		nums[i] = input;
	}
	reverse(nums, 10);
	return nums;
}